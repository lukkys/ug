package com.example.myapplication;

import android.app.Application;

import androidx.room.Room;

public class App extends Application {

    private static App instance;

    private AppDataBase appDataBase;

    @Override
    public void onCreate() {
        super.onCreate();

        instance = this;

        appDataBase = Room.databaseBuilder(
                getApplicationContext(),
                AppDataBase.class,
                "APP_DB_1"
        ).allowMainThreadQueries().build();
    }

    public static App getInstance() {
        return instance;
    }

    public AppDataBase getAppDataBase(){
        return appDataBase;
    }
}
