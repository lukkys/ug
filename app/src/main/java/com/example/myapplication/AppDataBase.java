package com.example.myapplication;


import androidx.room.Database;
import androidx.room.RoomDatabase;

@Database(entities = {Computer.class}, version = 1)
public abstract class AppDataBase extends RoomDatabase {
    abstract ComputerDao getComputerDao();
}
