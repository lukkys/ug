package com.example.myapplication;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import java.util.List;

@Dao
public interface ComputerDao {

    @Query("SELECT * FROM Computer")
    List<Computer> getComputers();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(Computer ... computers);

    @Delete
    void delete(Computer pc);

    @Query("DELETE FROM Computer")
    void deleteAll();

    @Query("SELECT * FROM Computer pc WHERE pc.COMPUTER_NAME LIKE :name AND pc.COMPUTER_RAM > :ram LIMIT 1")
    Computer nameAndRam(String name, Integer ram);


}
