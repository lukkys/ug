package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Computer pc = new Computer();
        pc.setComputerName("DELL");
        pc.setComputerRam(8);

        App.getInstance().getAppDataBase().getComputerDao().insert(pc);

        Computer computer = App.getInstance().getAppDataBase().getComputerDao().nameAndRam("ASUS", 4);
        Log.d("MyData", String.valueOf(computer));


    }
}