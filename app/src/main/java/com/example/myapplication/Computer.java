package com.example.myapplication;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity
public class Computer {

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "COMPUTER_ID")
    private Long computerId;

    @ColumnInfo(name = "COMPUTER_NAME")
    private String computerName;

    @ColumnInfo(name = "COMPUTER_RAM")
    private Integer computerRam;

    public Computer() {
    }

    public Long getComputerId() {
        return computerId;
    }

    public void setComputerId(Long computerId) {
        this.computerId = computerId;
    }

    public String getComputerName() {
        return computerName;
    }

    public void setComputerName(String computerName) {
        this.computerName = computerName;
    }

    public Integer getComputerRam() {
        return computerRam;
    }

    public void setComputerRam(Integer computerRam) {
        this.computerRam = computerRam;
    }

    @Override
    public String toString() {
        return "Computer{" +
                "computerId=" + computerId +
                ", computerName='" + computerName + '\'' +
                ", computerRam=" + computerRam +
                '}';
    }
}
